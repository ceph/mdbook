# mdBook docker image

Docker image to generate website from Markdown files using [mdBook](https://rust-lang-nursery.github.io/mdBook/)

## How to use

### Local development
```
cd my/doc/directory
docker run -ti --rm -p 3000:3000 -p 3001:3001 -v $PWD:/data gitlab-registry.cern.ch/ceph/mdbook:stable
xdg-open http://localhost:3000
```

### Push to S3
The image ships with `s3cmd`, so you can add something similar to the following in your `.gitlab-ci.yml`

```yaml
stages:
  - build
  - deploy

build:
  stage: build
  image: gitlab-registry.cern.ch/ceph/mdbook:stable
  script:
    - mdbook build
  artifacts:
    paths:
      - book

deploy-production:
  stage: deploy
  image: gitlab-registry.cern.ch/ceph/mdbook:stable
  only:
    - master
  script:
    - s3cmd --acl-public --access_key=$S3_ACCESS_KEY --secret_key=$S3_SECRET_KEY --host=s3.cern.ch --host-bucket='%(bucket)s.s3.cern.ch' --no-mime-magic --guess-mime-type sync ./book/ $S3_BUCKET_NAME/
```

Dont' forget to populate variable `S3_BUCKET_NAME` and secrets for `S3_ACCESS_KEY`, `S3_SECRET_KEY`