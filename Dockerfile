FROM alpine:latest

MAINTAINER Ceph Admins <ceph-admins@cern.ch>

# Switch to edge until s3cmd is in a stable release
RUN sed -i -e 's/v[[:digit:]]\.[[:digit:]]/edge/g' /etc/apk/repositories
RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories

ADD ./cargo/bin/ /usr/local/bin/
RUN apk --update-cache add s3cmd 

CMD ["/usr/local/bin/mdbook"]

WORKDIR /data
VOLUME /data

EXPOSE 3000

CMD /usr/local/bin/mdbook serve -n 0.0.0.0
